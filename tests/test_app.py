# -*- coding: utf-8 -*-

import os
import json

import pytest

import decryptmessage

TestingConfig = decryptmessage.config.TestingConfig
dirname = os.path.dirname
join = os.path.join
here = os.path.realpath(dirname(__file__))
examples = join(here, 'examples')


def get_message(number, result=False, extension='.gpg'):
    extension = '' if result else extension
    test_file = join(examples, 'test_file{}{}'.format(number+1, extension))

    with open(test_file, 'rb') as f:
        return f.read() if not result else f.read().decode().strip()


def load_parameters(amount=3, malformed=False):

    parameters = []

    for idx in range(amount):
        parameters.append((get_message(idx), get_message(idx, True)))

    return parameters


class TestApp(object):

    @classmethod
    def setup_class(cls):
        cls.message = get_message(1)
        cls.malformed_message = 'kfjldajj'
        cls.expected_result = get_message(1, True)
        cls.client = decryptmessage.app_factory(TestingConfig).test_client()

    def test_decrypt_message_endpoint(self):
        resp = self.client.post(
            '/decryptMessage',
            data={
                'Message': self.message,
                'Passphrase': 'topsecret'
            }
        )
        assert b'{"Message": "Cool beans"}\n' == resp.data

    def test_malformed_decryption_response(self):
        expected = {
            'status': 500,
            'message': 'Could not decrypt malformed message'
        }
        resp = self.client.post(
            '/decryptMessage',
            data={
                'Message': self.malformed_message,
                'Passphrase': 'topsecret'
            }
        )
        result = json.loads(resp.data.decode().strip())
        assert expected == result

    def test_not_found_response(self):
        expected = {
            'status': 404,
            'message': 'This is not a valid route. Try /decryptMessage'
        }
        resp = self.client.post('/wrongRoute')
        result = json.loads(resp.data.decode().strip())
        assert expected == result


@pytest.mark.parametrize(
    "data,expected_value",
    load_parameters(3)
)
def test_message_decrypting(data, expected_value):
    assert decryptmessage.decrypter.decrypt_message(data) == expected_value


def test_raises_on_malformed_messages():
    with pytest.raises(Exception):
        decryptmessage.decrypter.decrypt_message('jkfldk')

from setuptools import find_packages, setup

setup(
    author="Javis Sullivan",
    author_email="javissullivan@gmail.com",
    name="DecryptMessage",
    description="Decrypt pgp encrypted messages.",
    version="0.1.0",
    long_description="the docs",
    packages=find_packages("src"),
    package_dir={"": "src"},
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "flask",
        "flask-restful",
        "gunicorn",
        "python-gnupg",
        "pyaml"
    ],
    setup_requires=[
        "pytest-runner"
    ],
    tests_require=[
        "pytest",
        "pytest-flask"
    ],
    entry_points={
        "console_scripts": [
            "decryptmessage = decryptmessage.__main__:main"
        ]
    }
)

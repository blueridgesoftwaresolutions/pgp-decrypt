#!/bin/bash

if [ ! -f /usr/local/src/config_gnupg.sh ]; then
    cp /usr/local/src/app/server_configs/config_gnupg.conf \
    /usr/local/src/config_gnupg.sh
else
    /usr/local/src/config_gnupg.sh
fi

yum update -y
yum install -y python35 python35-virtualenv

virtualenv -p `which python3` --no-site-packages /usr/local/src/test-venv

source /usr/local/src/test-venv/bin/activate

pip install flask
pip install pytest
python setup.py install

pytest
deactivate
rm -rf /usr/local/src/test-venv

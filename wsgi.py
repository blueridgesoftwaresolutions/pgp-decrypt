#!/usr/bin/env python3
from decryptmessage.app import app_factory

app = app_factory()


if __name__ == '__main__':
    app.run()

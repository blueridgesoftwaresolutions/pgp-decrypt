#!/bin/bash

if [ ! -f ../config_gnupg ]; then

    cp ./config_gnugp.sh /usr/local/src/config_gnupg.sh

fi

if [ ! -f ../deploy.sh ]; then

    ln -s $0 /usr/local/src/
    ln -s ./run-tests.sh /usr/local/src/
fi

/usr/local/src/config_gnupg.sh

# Update distro
yum update -y

# install python35 and friends
yum install -y python35 python35-virtualenv
virtualenv -p `which python3` --no-site-packages /usr/local/src/venv
source /usr/local/src/venv/bin/activate
pip install pytest
pip install flask
pip install gunicorn
python setup.py install
export FLASK_CONFIG=production
/usr/local/src/venv/bin/gunicorn -w 3 -b 0.0.0.0:80 wsgi:app

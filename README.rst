README
======

This README would normally document whatever steps are necessary to get your application up and running.

What is this repository for?
----------------------------

* Quick summary
* Version


How do I get set up?
--------------------

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions


Contribution guidelines
-----------------------

* Writing tests
* Code review
* Other guidelines


Notes
-----

* Symmetric encryption is being used only.


Configuration
-------------

The ``decryptMessage`` service can be configured in 1 of 3 ways; production, development,
or testing. You may set the respective environments like so:

``export FLASK_CONFIG=production``

``export FLASK_CONFIG=development``

``export FLASK_CONFIG=testing``

  
Examples
--------

**Input**

::

  -----BEGIN PGP MESSAGE-----
  Version: GnuPG v2
  jA0ECQMCVady3RUyJw3X0kcBF+zdkfZOMhISoYBRwR3uk3vNv+TEg+rJnp4/yYIS
  pEoI2S82cDiCNBIVAYWB8WKPtH2R2YSussKhpSJ4mFgqyOA01uwroA==
  =KvJQ
  -----END PGP MESSAGE-----


**Output**

::

  gpg: AES256 encrypted data
  gpg: encrypted with 1 passphrase
  Nice work!



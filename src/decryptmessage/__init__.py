# -*- coding: utf-8 -*-

from . import app

__author__ = 'Javis Sullivan'
__name__ = 'decryptmessage'

app_factory = app.app_factory


__all__ = (
    "app_factory",
)

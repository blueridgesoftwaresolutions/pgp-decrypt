# -*- coding: utf-8 -*-

import flask_restful
from flask_restful import reqparse

from . import decrypter


parser = reqparse.RequestParser()
parser.add_argument('Message', type=str, help='No message found.')
parser.add_argument('Passphrase', type=str, help='No passhrase given.')


class DecryptMessage(flask_restful.Resource):

    def post(self):

        args = parser.parse_args()
        message = decrypter.decrypt_message(
            args['Message'],
            passphrase=args['Passphrase']
        )

        return {'Message': message}

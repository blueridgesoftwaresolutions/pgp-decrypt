# -*- coding: utf-8 -*-

import os

import flask
import flask_restful

from . import config
from . import exceptions
from . import routes


def get_config():
    '''Get config from ``FLASK_CONFIG`` env var.
       defaults to production.
    '''
    env = os.environ.get('FLASK_CONFIG', 'production')

    if env == 'production':
        return config.ProductionConfig
    elif env == 'development':
        return config.DevelopmentConfig
    else:
        return config.TestingConfig


def app_factory(config_object=None):
    '''Return app instance.
    '''

    FLASK_CONFIG = config_object if config_object is not None else get_config()

    app = flask.Flask(__name__)
    app.config.from_object(FLASK_CONFIG)
    api = flask_restful.Api(
        app,
        catch_all_404s=True,
        errors=exceptions.errors
    )
    api.add_resource(routes.DecryptMessage, '/decryptMessage')

    return app


if __name__ == '__main__':

    app_factory(config.DevelopmentConfig).run()

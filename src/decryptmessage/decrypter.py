# -*- coding: utf-8 -*-

import gnupg

from . import exceptions

gpg = gnupg.GPG(gnupghome='/home')


def decrypt_message(data, passphrase='topsecret'):
    try:
        message = gpg.decrypt(
            data,
            passphrase=passphrase
        ).data.decode().strip()
        assert len(message) != 0
    except Exception:
        raise exceptions.DecryptionError('Could not decrypt malformed message')

    return message

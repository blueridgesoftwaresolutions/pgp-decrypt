# -*- coding: utf-8 -*-

from werkzeug.exceptions import InternalServerError, NotFound


class DecryptionError(InternalServerError):
    pass


class InternalServerError(InternalServerError):
    pass


class NotFound(NotFound):
    pass


errors = {
    'DecryptionError': {
        'message': 'Could not decrypt malformed message',
        'status': 500
    },
    'InternalServerError': {
        'message': 'Could not decrypt message',
        'status': 500
    },
    'NotFound': {
        'message': 'This is not a valid route. Try /decryptMessage',
        'status': 404
    }
}

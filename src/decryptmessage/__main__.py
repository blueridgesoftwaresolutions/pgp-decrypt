#!/usr/bin/env python3

from . import app


def main(environ=None, resp=None):

    app.app_factory().run()


if __name__ == '__main__':
    main()

#!/bin/bash

# Setup gpg stuff
if [ ! -d /home/.gnupg ]; then

    mkdir -p /home/.gnupg
fi

cp ./server_configs/gpg-agent.conf /home/.gnupg/gpg-agent.conf

export GPG_TTY=`tty`
eval $( gpg-agent --daemon )
